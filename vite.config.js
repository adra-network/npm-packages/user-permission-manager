import terser from "@rollup/plugin-terser";
import { defineConfig } from "vite";
import { resolve } from "path";

export default defineConfig({
  resolve: {
    alias: {
      "@": resolve(__dirname, "./src"),
    },
  },
  build: {
    minify: true,
    lib: {
      entry: [
        resolve(__dirname, "src/index.js"),
        resolve(__dirname, "src/nuxt-plugin.js"),
        resolve(__dirname, "src/nuxt-module.js"),
      ],
      name: "lib",
    },
    rollupOptions: {
      plugins: [terser()],
      external: [],
      output: {
        globals: {},
        exports: "named",
      },
    },
  },
});
