import UserPermissionService from "./lib/userPermissionManager.js";

export default function ({ $auth, $sentry = null }, inject) {
  if (!$auth) {
    throw new Error("Auth module not found, it is required to use this plugin");
  }

  if ($sentry) {
    if (!$auth.loggedIn) {
      $sentry.configureScope((scope) => scope.setUser(null));
    }
    $sentry.setUser({
      id: $auth.user?.sub || null,
      email: $auth.user?.email || null,
      username: $auth.user?.preferred_username || null,
    });
  }

  inject(
    "user",
    new UserPermissionService(
      $auth?.user?.roles || $auth?.user?.realm_access?.roles || []
    )
  );
}
