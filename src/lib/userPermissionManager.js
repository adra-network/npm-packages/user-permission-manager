export default class UserPermissionManager {
  #tokenRoles = [];

  constructor(tokenRoles = []) {
    this.setRoles(tokenRoles);
  }

  can(roles, organization = "") {
    if (this.isSuperadmin) {
      return true;
    }
    roles = Array.isArray(roles) ? roles : [roles];
    return [...roles].some((role) => {
      if (organization) {
        return this.tokenRoles.includes(`${organization}_${role}`);
      }
      return this.tokenRoles.some((tokenRole) =>
        tokenRole.endsWith(`_${role}`)
      );
    });
  }
  get isSuperadmin() {
    return this.tokenRoles.includes("cloud_user_superadmin");
  }

  get roles() {
    return this.tokenRoles || [];
  }

  setRoles(roles = []) {
    this.tokenRoles = roles;
  }
}
