export default function (moduleOptions) {
  this.nuxt.hook("ready", async (nuxt) => {
    const pathy = require.resolve("./nuxt-plugin.js");
    this.nuxt.options.plugins.push(pathy);
  });
}

module.exports.meta = require("../package.json");
