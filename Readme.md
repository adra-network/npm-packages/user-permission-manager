# @adra-network/user-permission-manager

Frontend user permission manager is a package designed to manage UI elements based on user permissions. The module provides easy-to-use methods for handling different access levels and visibility of components.

## Features

- Show and hide UI components based on user permissions
- Flexibility to create complex permission structures
- Easy integration with existing frontend frameworks, specially optimized for Nuxt.js

## Installation

This module can be installed via npm. Run the following command:

```
npm install @adra-network/user-permission-manager
```

## Usage

After installation, import the module into your project:

```javascript
import UserManager from "@adra-network/user-permission-manager";
```

### Nuxt

To use this library as a nuxt plugin

- Add the following to your `modules` in your `nuxt.config.js` file

- Make sure it's loaded after the `@nuxtjs/auth-next` module as it needs it as a dependency

```javascript
modules: [
   '...',
   '@adra-network/user-permission-manager/nuxt-module',
 ],
```
