# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [7.2.1](https://gitlab.com/adra-network/npm-packages/user-permission-manager/compare/v7.2.0...v7.2.1) (2023-06-08)


### Bug Fixes

* **build:** Dist folder was excluded from VCS ([be0d54a](https://gitlab.com/adra-network/npm-packages/user-permission-manager/commit/be0d54accc8f295825c794189529c9b2ae601705))

## 7.2.0 (2023-06-08)


### Features

* **testing:** Implements testing and fix superadmin permission on can method ([cc051e1](https://gitlab.com/adra-network/npm-packages/user-permission-manager/commit/cc051e13452d4ebd6a802e56251ad36e9ef5f138))
