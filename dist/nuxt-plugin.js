import m from "./index.js";
function f({ $auth: e, $sentry: s = null }, i) {
  var u, l, n, o, r, t;
  if (!e)
    throw new Error("Auth module not found, it is required to use this plugin");
  s && (e.loggedIn || s.configureScope((a) => a.setUser(null)), s.setUser({ id: ((u = e.user) == null ? void 0 : u.sub) || null, email: ((l = e.user) == null ? void 0 : l.email) || null, username: ((n = e.user) == null ? void 0 : n.preferred_username) || null })), i("user", new m(((o = e == null ? void 0 : e.user) == null ? void 0 : o.roles) || ((t = (r = e == null ? void 0 : e.user) == null ? void 0 : r.realm_access) == null ? void 0 : t.roles) || []));
}
export {
  f as default
};
