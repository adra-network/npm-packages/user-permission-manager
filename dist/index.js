var i = (e, s, t) => {
  if (s.has(e))
    throw TypeError("Cannot add the same private member more than once");
  s instanceof WeakSet ? s.add(e) : s.set(e, t);
};
var o;
class u {
  constructor(s = []) {
    i(this, o, []);
    this.setRoles(s);
  }
  can(s, t = "") {
    return !!this.isSuperadmin || (s = Array.isArray(s) ? s : [s], [...s].some((r) => t ? this.tokenRoles.includes(`${t}_${r}`) : this.tokenRoles.some((n) => n.endsWith(`_${r}`))));
  }
  get isSuperadmin() {
    return this.tokenRoles.includes("cloud_user_superadmin");
  }
  get roles() {
    return this.tokenRoles || [];
  }
  setRoles(s = []) {
    this.tokenRoles = s;
  }
}
o = new WeakMap();
export {
  u as default
};
