function o(s) {
  this.nuxt.hook("ready", async (t) => {
    const e = require.resolve("./nuxt-plugin.js");
    this.nuxt.options.plugins.push(e);
  });
}
module.exports.meta = require("../package.json");
export {
  o as default
};
