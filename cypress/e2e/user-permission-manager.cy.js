import UserPermissionManager from "../../src/lib/userPermissionManager";

describe("superadmin testing", () => {
  it("it detects superadmin", () => {
    const roles = ["cloud_user_superadmin"];
    let manager = new UserPermissionManager(roles);
    expect(manager.isSuperadmin).to.be.true;
    manager = new UserPermissionManager([]);
    expect(manager.isSuperadmin).to.be.false;
  });

  it("a superadmin CAN do anything", () => {
    const roles = ["cloud_user_superadmin"];
    const manager = new UserPermissionManager(roles);
    expect(manager.isSuperadmin).to.be.true;
    expect(manager.can("something")).to.be.true;
  });
});

describe("roles testing", () => {
  it("it returns all roles", () => {
    const roles = ["cloud_user_superadmin", "some_role"];
    const manager = new UserPermissionManager(roles);
    expect(manager.roles.length).to.equal(roles.length);
  });
  it("it can set new roles", () => {
    const roles = ["cloud_user_superadmin", "some_role"];
    const manager = new UserPermissionManager(roles);
    expect(manager.roles.length).to.equal(roles.length);
    manager.setRoles(["new_role"]);
    expect(manager.roles.length).to.equal(1);
  });
});
